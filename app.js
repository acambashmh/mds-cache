var express = require('express');
var apicache = require('apicache').options({ debug: false }).middleware;
var request = require('request');
var fs = require('fs');


var app = express();

var token = fs.readFileSync('./token.txt', 'utf-8');

var tocUrlPart1 = 'https://www-k6.thinkcentral.com/api/ugen/toc?non_grade_level=&non_grade_title=&version=player&showResources=true&outputFormat=json'

function mdsCall(req, res) {

    var mdsUrl = tocUrlPart1 + '&program='+req.query.program + '&grade=' + req.query.grade

    var options = {
        url: mdsUrl,
        headers: {
            'Authorization': token
        }
    };
    request(options, function (err,response, body) {
        if(err){
            res.status(500);
            res.send();
            return;
        }
        res.send(body);
        
    });
        
}

app.get('/cach', apicache('1 hour'), mdsCall);

app.get('/nocach', mdsCall);

app.listen(3000);

